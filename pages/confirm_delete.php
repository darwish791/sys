<?php session_start(); 									?>

<?php if(!defined("URL")) define("URL", "/qaryah/"); 	?>

<?php include "../config.php";							?> <!--connection with database -->

<?php include "../pages/template/header.php";			?> 

<?php include "../pages/template/top-bar.php";			?>

<?php include "../pages/template/left-content.php";		?>


<?php
$table = 'ketua_keluarga';
$id = $_GET['id'];
//$no_kp_balik = $id;
//$no_kp_from = $id;
$i = 1;
//$_SESSION['test'] = $id;

//$query = "SELECT * FROM ketua_keluarga WHERE no_kp ='$id'";
//    $result = mysqli_query($connect, $query);
//    $count = mysqli_num_rows($result);
//
//    $row=mysqli_fetch_array($result);

	$query2 = "SELECT * FROM isteri_waris WHERE no_kp_ketua ='$id'";
    $result2 = mysqli_query($connect, $query2);
    $count2 = mysqli_num_rows($result2);

    $row2=mysqli_fetch_array($result2);

//$query3 = "SELECT * FROM tanggungan WHERE no_kp_ketua ='$id'";
//    $result3 = mysqli_query($connect, $query3);
//    $count3 = mysqli_num_rows($result3);


if (isset($_POST['save'])) {
$no_kp_ketua = isset($_POST['no_kp_ketua']) ? mysqli_real_escape_string($connect, $_POST['no_kp_ketua']) : '';

?>

<style>
	.custom-col {
		float:left;
		width:33.33%;

	}
</style>

<section class="content">
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
					<div class="header">
						<div class="table-responsive">
						<h1 align="center" style="color: red">ADAKAH ANDA PASTI INGIN MEMADAM DATA INI?</h1><br>
							<table class="table table-bordered table-striped table-hover dataTable js-exportable">
								<tr>
									<td width="20%"><img src="../johorloggo.png" alt="" style="display: block; margin-left: auto; margin-right: auto; width: 150px; height: 150px;"></td>
									<td width="60%" class="title" ><h4 align="center"><b><u>MAKLUMAT PERIBADI PENDUDUK KAMPUNG</u></b></h4>
									<br>
									<h4 align="center"><b>SISTEM PROFIL KAMPUNG<br>PERINGKAT NASIONAL(SPKPN)</b><br><i>(Unit Perancang Ekonomi Dengan Kerjasama <br> Kementerian Pembangunan Luar Bandar)</i></h4>
									</td>
									<td width="20%"></td>
								</tr>
							</table>
						</div>
					</div>
					<div class="body">
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover dataTable js-exportable">	
							<tr class="spaceUnder">
								<td width="17%">Nama</td>
								<td width="1%">:&nbsp;</td>
								<td width="31%"><?php echo $row2['nama'] ?></td>
								<td width="17%">No K/P</td> 
								<td width="1%">:&nbsp;</td>
								<td width="31%"><?php echo $row2['no_kp'] ?></td>
							</tr>
							<tr class="spaceUnder">
								<td>Tarikh Lahir</td> 
								<td>:&nbsp;</td>
								<td><?php echo $row2['tarikh_lahir'] ?></td>
								<td>Jantina</td>
								<td>:&nbsp;</td>
								<td><?php echo $row2['jantina'] ?></td>
							</tr>
							<tr class="spaceUnder">
								<td>Tempat Kelahiran</td>
								<td>:&nbsp;</td>
								<td><?php echo $row2['tempat_lahir'] ?></td>
								<td>No. HP</td> 
								<td>:&nbsp;</td>
								<td><?php echo $row2['no_hp'] ?></td>
							</tr>
							<tr class="spaceUnder">
								<td>Pekerjaan</td>
								<td>:&nbsp;</td>			
								<td><?php echo $row2['pekerjaan'] ?></td>
								<td>Pendapatan Bulanan</td><br>
								<td>:&nbsp;</td>
								<td><?php echo $row2['pendapatan'] ?></td>
							</tr>
							
							<tr class="spaceUnder">
								<td>Tahap Pendidikan</td>
								<td>:&nbsp;</td>			
								<td><?php echo $row2['pendidikan'] ?></td>
							</tr>
							</table>
						</div>
						<div>
							<form method="post"  style="text-align:center" action="confirm_delete.php?deleteisteri=<?php echo $id; ?>" >
									<input type="submit" name="save" value="Padam" class="link btn btn-danger">
									<a type="link" href="ketua_view.php?id=<?php echo $id?>" class="btn btn-primary">Batal</a>
									<input type="hidden" name="no_kp_ketua" value="<?php echo $id; ?>" placeholder="No Kad Pengenalan" required>
							</form>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
</section>


<?php
if(isset($_GET['deleteisteri'])){
    
    $padam_isteri = $_GET['deleteisteri'];
    $query4 = "DELETE FROM isteri_waris WHERE no_kp_ketua = $padam_isteri";
    $result4 = mysqli_query($connect, $query4);
    
    if(!$result4){
        die("DELETE failed" . mysqli_error($connect));
    }else{
        echo "<script>location.href='ketua_view.php?id=$no_kp_ketua'</script>";
    }
}
}
?>

<?php include "../pages/template/footer.php"; ?>

