<?php session_start(); 									?>

<?php //if(!defined("URL")) define("URL", "/qaryahnet/v1/sys"); 	?>
<?php if(!defined("URL")) define("URL", "/sys"); 	?>

<?php include "../config.php";							?> <!--connection with database -->

<?php include "template/header.php";			?> 

<?php include "template/top-bar.php";			?>

<?php include "template/left-content.php";		?>


<?php
$table = 'tanggungan';
$id = $_GET['id'];
$nama_ketua = $_GET['nama'];
$no_kp_ketua = $id;
$saved = 0;

$_POST['save'] = NULL;

foreach ($_POST as $key=>$value){
    if ($value==''){
        $_POST[$key]='0';
    }
}
$no_kp_ketua = isset($_POST['no_kp_ketua']) ? mysqli_real_escape_string($connect, $_POST['no_kp_ketua']) : '';

$no_kp = isset($_POST['no_kp']) ? mysqli_real_escape_string($connect, $_POST['no_kp']) : '';
$nama = isset($_POST['nama']) ? mysqli_real_escape_string($connect, $_POST['nama']) : '';
$tarikh_lahir = isset($_POST['tarikh_lahir']) ? mysqli_real_escape_string($connect, $_POST['tarikh_lahir']) : '';
$jantina = isset($_POST['jantina']) ? mysqli_real_escape_string($connect, $_POST['jantina']) : '';
$tempat_lahir = isset($_POST['tempat_lahir']) ? mysqli_real_escape_string($connect, $_POST['tempat_lahir']) : '';
$pekerjaan = isset($_POST['pekerjaan']) ? mysqli_real_escape_string($connect, $_POST['pekerjaan']) : '';
$pendapatan = isset($_POST['pendapatan']) ? mysqli_real_escape_string($connect, $_POST['pendapatan']) : '';
$pendidikan = isset($_POST['pendidikan']) ? mysqli_real_escape_string($connect, $_POST['pendidikan']) : '';
$pusat_pendidikan = isset($_POST['pusat_pendidikan']) ? mysqli_real_escape_string($connect, $_POST['pusat_pendidikan']) : '';
$anak_yatim = isset($_POST['anak_yatim']) ? mysqli_real_escape_string($connect, $_POST['anak_yatim']) : '';
$asnaf = isset($_POST['asnaf']) ? mysqli_real_escape_string($connect, $_POST['asnaf']) : '';
$status = isset($_POST['status']) ? mysqli_real_escape_string($connect, $_POST['status']) : '';

$nama = strtoupper($nama);
$tempat_lahir = strtoupper($tempat_lahir);
$pekerjaan = strtoupper($pekerjaan);
$pusat_pendidikan = strtoupper($pusat_pendidikan);

$waktu_semasa = date("Y-m-d");
$diff = date_diff(date_create($tarikh_lahir), date_create($waktu_semasa));
$umur = $diff->format('%y');

if (isset($_POST['save'])) {
    $query = "INSERT INTO $table (no_kp, nama, tarikh_lahir, tanggungan_umur, jantina, pekerjaan, pendapatan, pendidikan,pusat_pendidikan, anak_yatim, asnaf, tanggungan_status, no_kp_ketua) VALUES ('$no_kp', '$nama', '$tarikh_lahir', $umur, '$jantina', '$pekerjaan', $pendapatan, '$pendidikan', '$pusat_pendidikan', '$anak_yatim', '$asnaf', '$status', '$no_kp_ketua')";
    $result = mysqli_query($connect, $query); 
    //isset($result) ? $message = '<p class="message">Data saved</p> ' . $no_kp : $message = '';

}
 
if ($no_kp != "") {
     echo "<script>location.href='ketua_view.php?id=$no_kp_ketua'</script>";
    exit;
}

?>

<style>
	.custom-col {
		float:left;
		width:33.33%;

	}
</style>

<section class="content">
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
					<div class="header">
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover dataTable js-exportable">
								<tr>
									<td width="20%"><img src="../johorloggo.png" alt="" style="display: block; margin-left: auto; margin-right: auto; width: 150px; height: 150px;"></td>
									<td width="60%" class="title" ><h4 align="center"><b><u>MAKLUMAT PERIBADI PENDUDUK KAMPUNG</u></b></h4>
									<br>
									<h4 align="center"><b>SISTEM PROFIL KAMPUNG<br>PERINGKAT NASIONAL(SPKPN)</b><br><i>(Unit Perancang Ekonomi Dengan Kerjasama <br> Kementerian Pembangunan Luar Bandar)</i></h4>
									</td>
									<td width="20%"></td>
								</tr>
							</table>
						</div>
					</div>
					<div class="body">
						<form method="post" action="">
							<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover dataTable js-exportable">
									<div>
									<b><i>C - Maklumat Anak Dalam Tanggungan Yang Belum Berkahwin :</i></b>
									</div>
									<br><br>Nama Ketua Keluarga: <?php echo $nama_ketua; ?>
									<input type="hidden" name="no_kp_ketua" value="<?php echo $id; ?>" placeholder="No Kad Pengenalan" required>
									<br>
									
									<tr class="spaceunder">
									<td>Nama</td>
									<td>:</td>
									<td><input type="text" name="nama" id="" class="form-control" autocomplete="off" required></td>
									<td>&nbsp; No K/P</td> 
									<td>:</td>
									<td><input type="text" name="no_kp" id="" class="form-control" autocomplete="off" required placeholder="Contoh: 921205015457"></td>
								</tr>
								<tr class="spaceunder">
									<td>Tarikh Lahir</td> 
									<td>:</td>
									<td><input type="date" autocomplete="off" name="tarikh_lahir" id="" class="form-control"></td>
									<td>&nbsp; Jantina</td> 
									<td>:</td>
									<td>
									<input id="LELAKI" class="with-gap" type="radio" name="jantina" value="LELAKI" required><label for="LELAKI">LELAKI</label>
									<input id="PEREMPUAN" class="with-gap" type="radio" name="jantina" value="PEREMPUAN" required><label for="PEREMPUAN">PEREMPUAN</label>
									</td>
								</tr>
								
								<tr class="spaceunder">
									<td>Pekerjaan</td>
									<td>:</td>			
									<td><input type="text" name="pekerjaan" id="" class="form-control" autocomplete="off"></td>
									<td>&nbsp; Pendapatan Bulanan</td> 
									<td>:</td>
									<td><input type="text" name="pendapatan" id="" class="form-control" autocomplete="off" placeholder="Contoh: 1200.50"></td>
								</tr>
								
								<tr class="spaceunder">
									<td>Tahap Pendidikan</td>
									<td>:</td>			
									<td>
									<select class="form-control" id="pilihan" name="pendidikan" required>
									  <option value="">-- Pilih --</option>
										<option value="TIDAK BERSEKOLAH">TIDAK BERSEKOLAH</option>
									  <option value="UPSR">UPSR</option>
									  <option value="PT3/SRP/LCE">PT3/SRP/LCE</option>
									  <option value="SPM/MCE">SPM/MCE</option>
									  <option value="STPM">STPM</option>
									  <option value="DIPLOMA">DIPLOMA</option>
									  <option value="SARJANA MUDA">SARJANA MUDA</option>
									  <option value="SARJANA">SARJANA</option>
									  <option value="PhD">PhD</option>
									</select>
									</td>
									<td>&nbsp; Pusat Pendidikan</td> 
									<td>:</td>
									<td><input type="text" name="pusat_pendidikan" id="" class="form-control"></td>
								</tr>

								<tr class="spaceunder">
									<td>&nbsp; Anak Yatim</td> 
									<td>:</td>
									<td>
									<input id="radio_anak_yatim_ya" class="with-gap" type="radio" name="anak_yatim" value="ANAK YATIM" required><label for="radio_anak_yatim_ya">YA</label>
									<input id="radio_anak_yatim_tidak" class="with-gap" type="radio" name="anak_yatim" value="BUKAN ANAK YATIM" required><label for="radio_anak_yatim_tidak">TIDAK</label>
									</td>
									<td>&nbsp; Asnaf Zakat</td> 
									<td>:</td>
									<td>
										<input id="radio_asnaf_ya" class="with-gap" type="radio" name="asnaf" value="ASNAF" required><label for="radio_asnaf_ya">YA</label>
										<input id="radio_asnaf_tidak" class="with-gap" type="radio" name="asnaf" value="BUKAN ASNAF" required><label for="radio_asnaf_tidak">TIDAK</label>
									</td>
								</tr>

								<tr>
										<td>Status</td>
										<td>:</td>
										<td>
											<input id="status_ya" class="with-gap" type="radio" name="status" value="MASIH HIDUP" required><label for="status_ya">MASIH HIDUP</label>
											<input id="status_tidak" class="with-gap" type="radio" name="status" value="SUDAH MENINGGAL" required><label for="status_tidak">SUDAH MENINGGAL</label>
										</td>	
									</tr>

								</table>
							</div>
							<div align="center">
								<input type="submit" name="save" value="Hantar" class="link btn btn-success">
							</div>
						</form>
					</div>
				</div>
			</div>
        </div>
    </div>
</section>

<?php include "template/footer.php"; ?>

