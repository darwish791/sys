<?php session_start(); 									?>

<?php //if(!defined("URL")) define("URL", "/qaryahnet/v1/sys"); 	?>
<?php if(!defined("URL")) define("URL", "/sys"); 	?>

<?php include "../config.php";							?> <!--connection with database -->

<?php include "template/header.php";			?> 

<?php include "template/top-bar.php";			?>

<?php include "template/left-content.php";		?>

<?php 

$qaryah_id = isset($_POST['qaryah_id']) ? mysqli_real_escape_string($connect, $_POST['qaryah_id']) : '';
 ?>

<?php 
	$query = "SELECT * FROM ketua_keluarga WHERE jantina = 'LELAKI' AND ketua_umur BETWEEN 15 AND 70";
	$result = mysqli_query($connect, $query);
	$count = mysqli_num_rows($result);

	$query_waris = "SELECT * FROM isteri_waris WHERE jantina = 'LELAKI' AND isteri_umur BETWEEN 15 AND 70";
	$result_waris = mysqli_query($connect, $query_waris);
	$count_waris = mysqli_num_rows($result_waris);

	$query_tanggungan = "SELECT * FROM tanggungan WHERE jantina = 'LELAKI' AND tanggungan_umur BETWEEN 15 AND 70";
	$result_tanggungan = mysqli_query($connect, $query_tanggungan);
	$count_tangungan = mysqli_num_rows($result_tanggungan);
?>

<style>
	.custom-col {
		float:left;
		width:33.33%;

	}
</style>

<section class="content">
	<div class="container-fluid">
		<div class="row clearfix">
		
			<div class="col-xs-20 col-sm-20 col-md-18 col-lg-18">
				<div class="card">
					<div class="body">
						<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
							<!-- Indicators -->
							<ol class="carousel-indicators">
									<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
									<li data-target="#carousel-example-generic" data-slide-to="1"></li>
									<li data-target="#carousel-example-generic" data-slide-to="2"></li>
									<li data-target="#carousel-example-generic" data-slide-to="3"></li>
									<li data-target="#carousel-example-generic" data-slide-to="4"></li>
									<li data-target="#carousel-example-generic" data-slide-to="5"></li>
							</ol>

							<!-- Wrapper for slides -->
							<div class="carousel-inner" role="listbox" align="center">
									<div class="item active">
											<img src="../images/masjid/2.png" />
									</div>
									<div class="item">
											<img src="../images/masjid/a.jpg" />
									</div>
									<div class="item">
											<img src="../images/masjid/b.jpg" />
									</div>
									<div class="item">
											<img src="../images/masjid/c.jpg" />
									</div>
									<div class="item">
											<img src="../images/masjid/d.jpg" />
									</div>
									<div class="item">
										<img src="../images/masjid/e.jpg" />
									</div>
							</div>

							<!-- Controls -->
							<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
									<span class="sr-only">Previous</span>
							</a>
							<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
									<span class="sr-only">Next</span>
							</a>
						</div>
					</div>
				</div>
			</div>
			<!-- #END# Basic Example -->
		
		
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
					<div class="header">
						<h2>
							Senarai Wajib Solat Jumaat - <?php echo $count + $count_waris + $count_tangungan; ?>
						</h2>
						<p>
							Umur 15 - 70
						</p>
					</div>
					<div class="body">
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover dataTable js-exportable">
								
								<!--<table class="paleBlueRows">-->
									<thead>
										<tr>
												<th>Bilangan</th>
												<th>Nama</th>
												<th>Jantina</th>
												<th>Umur</th>
												<th>Pendapatan</th>
												<th>Pekerjaan</th>
												<th>Pendidikan</th>
												<th>Lihat</th>
										</tr>
									</thead>
									<tbody>
										<?php
										if ($count != 0) {
											$k = 1;
											while ($row = mysqli_fetch_array($result)):
												?>
												<tr>
														<td width="10%" style="text-align:center"><?php echo $k; ?></td>
														<td width="15%"><?php echo $row['nama']; ?></td>
														<td width="15%"><?php echo $row['jantina']; ?></td>
														<td width="15%"><?php echo $row['ketua_umur']; ?></td>
														<td width="15%"><?php echo $row['pendapatan']; ?></td>
														<td width="10%" style="text-align:center"><?php echo $row['pekerjaan']; ?></td>
														<td width="10%" style="text-align:center"><?php echo $row['pendidikan']; ?></td>
														<td width="5%" style="text-align:center">
															<a href="ketua_view.php?id=<?php echo $row['no_kp']; ?> " class="btn btn-primary" disabled>Lihat</a>  
														</td>
											 
												</tr>
												<?php
												$k++;
											endwhile;
										}
										if ($count_waris != 0) {
											while ($row_waris = mysqli_fetch_array($result_waris)):
												?>
												<tr>
														<td width="10%" style="text-align:center"><?php echo $k; ?></td>
														<td width="15%"><?php echo $row_waris['nama']; ?></td>
														<td width="15%"><?php echo $row_waris['jantina']; ?></td>
														<td width="15%"><?php echo $row_waris['isteri_umur']; ?></td>
														<td width="15%"><?php echo $row_waris['pendapatan']; ?></td>
														<td width="10%" style="text-align:center"><?php echo $row_waris['pekerjaan']; ?></td>
														<td width="10%" style="text-align:center"><?php echo $row_waris['pendidikan']; ?></td>
														<td width="5%" style="text-align:center">
															<a href="ketua_view.php?id=<?php echo $row['no_kp']; ?> " class="btn btn-primary" disabled>Lihat</a>  
														</td>
											 
												</tr>
												<?php
												$k++;
											endwhile;
										}
										if ($count_tangungan != 0) {
											while ($row_tangungan = mysqli_fetch_array($result_tanggungan)):
												?>
												<tr>
														<td width="10%" style="text-align:center"><?php echo $k; ?></td>
														<td width="15%"><?php echo $row_tangungan['nama']; ?></td>
														<td width="15%"><?php echo $row_tangungan['jantina']; ?></td>
														<td width="15%"><?php echo $row_tangungan['tanggungan_umur']; ?></td>
														<td width="15%"><?php echo $row_tangungan['pendapatan']; ?></td>
														<td width="10%" style="text-align:center"><?php echo $row_tangungan['pekerjaan']; ?></td>
														<td width="10%" style="text-align:center"><?php echo $row_tangungan['pendidikan']; ?></td>
														<td width="5%" style="text-align:center">
															<a href="ketua_view.php?id=<?php echo $row['no_kp']; ?> " class="btn btn-primary" disabled>Lihat</a>  
														</td>
											 
												</tr>
												<?php
												$k++;
											endwhile;
										}else {
											?>
											<tr>
												<td colspan="5" style="text-align: center;">Data not found</td>
											</tr>
											<?php
										}
										?>
										<?php
										if(isset($_GET['delete'])){
																	
											$padam_kp = $_GET['delete'];
											$query = "DELETE FROM $table WHERE no_kp = $padam_kp";
											$result = mysqli_query($connect, $query);
											
											$query2 = "DELETE FROM isteri_waris WHERE no_kp_ketua = $padam_kp";
											$result2 = mysqli_query($connect, $query2);
											
											$query3 = "DELETE FROM tanggungan WHERE no_kp_ketua = $padam_kp";
											$result3 = mysqli_query($connect, $query3);
											
											echo "<script>location.href='home.php'</script>";
											if(!$result){
											die("DELETE failed" . mysqli_error($connect));
											}
										}
										?>
									</tbody>
								<!--</table>-->
							</table>
						</div>
					</div>
				</div>
			</div>
		<div>
	</div>
</section>
<?php include "../pages/template/footer.php"; ?>
</body>

</html>
